import XLSX from 'xlsx';

export default async function excelReader(file){
  const reader = new FileReader();
  reader.addEventListener('load', (e: $TSFixMe) => {
    const data = e.target.result;
    const workbook = XLSX.read(data, {
      type: 'binary',
    });

    workbook.SheetNames.forEach((sheetName) => {
      const JSONRows = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {
        header: 1
      });

      console.log(JSONRows)
    });
  });

  reader.addEventListener('error', () => {
    console.log('ERROR')
  });

  reader.readAsBinaryString(file);

}